#include <iostream>
#include <cmath>

class Example
{
private:
	int a;
public:
	Example() : a(1)
	{}

	Example(int newA) : a(newA)
	{}

	int GetA()
	{
		return a;
	}

	void SetA(int newA)
	{
		a = newA;
	}
};

class Vector
{
public:
	Vector() : x(7), y(7), z(7)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		std::cout << "\n" << x << " " << y << " " << z;
	}
	
	void length()
	{
		double result = (sqrt((x * x) + (y * y) + (z * z)));
		std::cout << "\n" << result;
	}

private:
	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
	Vector v;
	v.Show();
	v.length();
	Example temp(11);
	std::cout <<"\n"<< temp.GetA();

}
